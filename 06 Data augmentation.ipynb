{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b79eaf00-72d1-4692-a7d8-a3ab93045a95",
   "metadata": {},
   "source": [
    "# Data augmentation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce8938de-0d72-4fc1-93f3-74ab72ce543f",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "The problem we're seeing here is caused by our training set being a bit restrictive. The network can only learn from what we show it, so if we want it to be able to understand black-on-white writing as well as white-on-black then we need to show it some labelled examples of *that* too.\n",
    "\n",
    "If you're training your network to recognise dogs then you don't just want good-looking, well-lit photos of dogs straight on. You want to be able to recognise a variety of angles, lighting conditions, framings etc. Some of these can only be improved by supplying a wider range of input (e.g. by taking new photos) but you can go a long way to improving your resiliency to test data by automatically creating new examples by inverting, blurring, rotating, adding noise, scaling etc. your training data. This is known as *data augmentation*.\n",
    "\n",
    "In general, data augmentation is an important part of training any network but it is particularly useful for CNNs."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac0fcb72-129e-48de-89de-22bdd10a660b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Inverting the images\n",
    "\n",
    "In our case we're going to simply add colour-inverted versions of the data to our training data set.\n",
    "\n",
    "We use the `Dataset.map()` and `Dataset.concatenate()` methods to double up our training set with a set of images where all the values have been inverted in the range 0-1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2bea6b66-174b-4d60-b7c7-6941ca7276fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def invert_img(image, label):\n",
    "    return 1.-image, label"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a305d56-100a-4d26-be2e-40317ad78193",
   "metadata": {},
   "source": [
    "Then, to the data preparation, add in a line like\n",
    "\n",
    "```python\n",
    "ds = ds.concatenate(ds.map(invert_img))\n",
    "```\n",
    "\n",
    "just after the image normalisation `map`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8a514bb2-8649-407f-9caa-ae591d1ac211",
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2024-01-19 10:52:54.038470: I external/local_tsl/tsl/cuda/cudart_stub.cc:31] Could not find cuda drivers on your machine, GPU will not be used.\n",
      "2024-01-19 10:52:54.079722: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered\n",
      "2024-01-19 10:52:54.079757: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered\n",
      "2024-01-19 10:52:54.080605: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered\n",
      "2024-01-19 10:52:54.086023: I external/local_tsl/tsl/cuda/cudart_stub.cc:31] Could not find cuda drivers on your machine, GPU will not be used.\n",
      "2024-01-19 10:52:55.094465: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT\n",
      "2024-01-19 10:52:56.243420: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:901] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero. See more at https://github.com/torvalds/linux/blob/v6.0/Documentation/ABI/testing/sysfs-bus-pci#L344-L355\n",
      "2024-01-19 10:52:56.248910: W tensorflow/core/common_runtime/gpu/gpu_device.cc:2256] Cannot dlopen some GPU libraries. Please make sure the missing libraries mentioned above are installed properly if you would like to use GPU. Follow the guide at https://www.tensorflow.org/install/gpu for how to download and setup the required libraries for your platform.\n",
      "Skipping registering GPU devices...\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "model = tf.keras.models.Sequential([\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=16,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=32,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "    tf.keras.layers.Flatten(),\n",
    "    tf.keras.layers.Dense(128, activation=\"relu\"),\n",
    "    tf.keras.layers.Dropout(0.4),\n",
    "    tf.keras.layers.Dense(10, activation=\"softmax\")\n",
    "])\n",
    "\n",
    "model.compile(\n",
    "    loss=\"sparse_categorical_crossentropy\",\n",
    "    metrics=[\"accuracy\"],\n",
    ")\n",
    "\n",
    "\n",
    "import tensorflow_datasets as tfds\n",
    "\n",
    "def normalize_img(image, label):\n",
    "    return tf.cast(image, tf.float32) / 255., label\n",
    "\n",
    "ds_train, ds_test = tfds.load(\n",
    "    \"mnist\",\n",
    "    split=[\"train\", \"test\"],\n",
    "    as_supervised=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ef14b450-878b-46e5-b63a-cd1d0e5f9f79",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_train = ds_train.map(normalize_img)\n",
    "ds_train = ds_train.concatenate(ds_train.map(invert_img))  # new line\n",
    "ds_train = ds_train.shuffle(1000)\n",
    "ds_train = ds_train.batch(128)\n",
    "\n",
    "ds_test = ds_test.map(normalize_img)\n",
    "ds_test = ds_test.concatenate(ds_test.map(invert_img))  # new line\n",
    "ds_test = ds_test.batch(128)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc380a2b-b707-469b-8ebe-d4c5fd5cf76f",
   "metadata": {},
   "source": [
    "If you then retrain the model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "f1b3f115-79b4-402d-b814-f1537293eea2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/2\n",
      "938/938 [==============================] - 56s 58ms/step - loss: 0.2039 - accuracy: 0.9366 - val_loss: 0.1908 - val_accuracy: 0.9368\n",
      "Epoch 2/2\n",
      "938/938 [==============================] - 55s 58ms/step - loss: 0.0720 - accuracy: 0.9782 - val_loss: 0.0539 - val_accuracy: 0.9823\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<keras.src.callbacks.History at 0x7f7d90166190>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model.fit(\n",
    "    ds_train,\n",
    "    validation_data=ds_test,\n",
    "    epochs=2,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a99ea67-a94b-4765-b711-6384e84b7627",
   "metadata": {},
   "source": [
    "You should see an improvement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "182f4706-77ec-47cc-80b3-321184c04252",
   "metadata": {
    "tags": [
     "remove_input"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1/1 [==============================] - 0s 89ms/step\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<table cellpadding=\"0\" style=\"border-collapse: collapse; border-style: hidden;\">\n",
       "    <thead>\n",
       "    <tr>\n",
       "    <td><b>Image</b></td><td><b>0</b></td><td><b>1</b></td><td><b>2</b></td><td><b>3</b></td><td><b>4</b></td><td><b>5</b></td><td><b>6</b></td><td><b>7</b></td><td><b>8</b></td><td><b>9</b></td></tr>\n",
       "    </thead>\n",
       "    <tbody><tr><td><img src=\"1.png\" style=\"margin: 1px 0px\"></td><td>4%</td><td style=\"color:green;\">61%</td><td>10%</td><td>2%</td><td>5%</td><td>3%</td><td>5%</td><td>3%</td><td>6%</td><td>2%</td></tr><tr><td><img src=\"2.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"3.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"4.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"5.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"6.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"7.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>35%</td><td>1%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">64%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"8.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>1%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">99%</td><td>0%</td></tr><tr><td><img src=\"9.png\" style=\"margin: 1px 0px\"></td><td>31%</td><td>0%</td><td>1%</td><td>0%</td><td>1%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:red;\">62%</td><td>4%</td></tr><tr><td><img src=\"dog.png\" style=\"margin: 1px 0px\"></td><td>6%</td><td>4%</td><td>15%</td><td>2%</td><td>1%</td><td>13%</td><td>18%</td><td>2%</td><td style=\"color:red;\">39%</td><td>0%</td></tr></tbody>\n",
       "    </table>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "from skimage.io import imread\n",
    "\n",
    "images = []\n",
    "for i in list(range(1,10)) + [\"dog\"]:\n",
    "    images.append(np.array(imread(f\"{i}.png\")/255.0, dtype=\"float32\"))\n",
    "images = np.array(images)[:,:,:,np.newaxis]\n",
    "test_data = tf.convert_to_tensor(images)\n",
    "\n",
    "probabilities = model.predict(test_data)\n",
    "\n",
    "truths = list(range(1, 10)) + [\"dog\"]\n",
    "\n",
    "table = []\n",
    "for truth, probs in zip(truths, probabilities):\n",
    "    prediction = probs.argmax()\n",
    "    table.append((truth, probs))\n",
    "    \n",
    "from IPython.display import display, HTML\n",
    "\n",
    "def print_table(table):\n",
    "    out = \"\"\n",
    "    out += \"\"\"<table cellpadding=\"0\" style=\"border-collapse: collapse; border-style: hidden;\">\n",
    "    <thead>\n",
    "    <tr>\n",
    "    <td><b>Image</b></td>\"\"\"\n",
    "    for i in range(10):\n",
    "        out += f\"<td><b>{i}</b></td>\"\n",
    "    out += \"\"\"</tr>\n",
    "    </thead>\n",
    "    <tbody>\"\"\"\n",
    "    for truth, l in table:\n",
    "        out += \"<tr>\"\n",
    "        out += f'<td><img src=\"{truth}.png\" style=\"margin: 1px 0px\"></td>'\n",
    "        highest_prob = l.argmax()\n",
    "        for j, m in enumerate(l):\n",
    "            if j == highest_prob:\n",
    "                if highest_prob == truth:\n",
    "                    colour = \"green\"\n",
    "                else:\n",
    "                    colour = \"red\"\n",
    "                out += f'<td style=\"color:{colour};\">{int(round(m*100))}%</td>'\n",
    "            else:\n",
    "                out += f\"<td>{int(round(m*100))}%</td>\"\n",
    "        out += \"</tr>\"\n",
    "    out += \"\"\"</tbody>\n",
    "    </table>\"\"\"\n",
    "    return HTML(out)\n",
    "\n",
    "print_table(table)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be6b7f93-958d-49ca-b068-5887493b78bf",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Summary\n",
    "\n",
    "It's possible that you only see a small improvement and even a worsening on some examples. Particularly on the `9` example, the network will struggle as it doesn't really represent the training data set. Here are some things that may improve network performance:\n",
    "\n",
    " - More data augmentation (brightness, rotations, blurring etc.)\n",
    " - Larger base training set (colour images perhaps)\n",
    " - Larger number of training epochs (in general, the more the better)\n",
    " - Tweak the hyperparameters (dropout rate, learning rate, kernel size, number of filters, etc.)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
