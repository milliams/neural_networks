{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6b8ea45c-ef2d-4885-bd1d-6707356b7b2b",
   "metadata": {},
   "source": [
    "# Neural networks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "326f3002-8ba7-48d8-a834-5a73b2fcffd5",
   "metadata": {
    "tags": []
   },
   "source": [
    "Neural networks are a collection of artificial neurons connected together so it's best to start by learning about about neurons.\n",
    "\n",
    "In nature, a neuron is a cell which has an electrical connection to other neurons. If a charge is felt from 'enough' of the input neurons then the neuron fires and passes a charge to its output. This design and how they are arranged into networks is the direct inspiration for artificial neural networks.\n",
    "\n",
    "An artificial neuron has multiple inputs and can pass its output to multiple other neurons.\n",
    "\n",
    "A neuron will calculate its value, $p = \\sum_i{x_iw_i}$ where $x_i$ is the input value and $w_i$ is a weight assigned to that connection. This $p$ is then passed through some *activation function*, $\\phi()$, to determine the output of the neuron. The activation function's job is to introduce some non-linearity between layer, allowing you to solve more complex problems.\n",
    "\n",
    "<img src=\"neuron.png\" alt=\"An artificial neuron\" style=\"width: 200px; margin:0 auto;\"/>\n",
    "\n",
    "The final equation for a node is therefore:\n",
    "\n",
    "$$o = \\phi(\\sum_i{x_iw_i})$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56bed905-df45-4ac5-be61-76246abfd763",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Networks\n",
    "\n",
    "The inputs to each neurons either come from the outputs of other neurons or are explicit inputs from the user. This allows you to connect together a large network of neurons:\n",
    "\n",
    "<img src=\"network.png\" alt=\"An artificial neural network\" style=\"width: 400px; margin:0 auto;\"/>\n",
    "\n",
    "In this network every neuron on one layer is connected to every neuron on the next. Every arrow in the diagram has a weight assigned to it.\n",
    "\n",
    "You input values on the left-hand side of the network, and the data flows through the network from layer to layer until the output layer has a value."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b13b5e17-0f5d-49f1-8f87-45464e2ae1a5",
   "metadata": {
    "tags": []
   },
   "source": [
    "### What shape should the network be?\n",
    "\n",
    "There is some art and some science to deciding the shape of a network. There are rules of thumb (hidden layer size should be similar sized to the input and output layers) but this is one of the things that you need to experiment with and see how it affects performance.\n",
    "\n",
    "The number of hidden layers relates to the level of abstraction you are looking at. Generally, more complex problems need more hidden layers (i.e. deeper networks) but this makes training harder.\n",
    "\n",
    "### How are the weights calculated?\n",
    "\n",
    "The calculation of the weights in a network is done through a process called *training*. This generally uses lots of data examples to iteratively work out good values for the weights."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cff7a195-5834-49ca-a0f7-b25bc1144137",
   "metadata": {
    "tags": []
   },
   "source": [
    "## How do you train neural networks\n",
    "\n",
    "The main method by which NNs are trained is a technique called *backpropagation*.\n",
    "\n",
    "In order to train your network you need a few things:\n",
    " - A labelled training data set\n",
    " - A labelled test (or evaluation) data set\n",
    " - A set of initial weights"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d47cd84d-4995-40a1-8bc3-b98d6d9e2692",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Initial weights\n",
    "\n",
    "The weights to start with are easy: just set them randomly!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e996bef-b582-47fc-8c24-2e5b1c33f5f6",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Training and testing data sets\n",
    "\n",
    "You will need two data sets. One will be used by the learning algorithm to train the network and the other will be used to report on the quality of the training at the end.\n",
    "\n",
    "It is important that these data sets are disjoint to prevent *overfitting*.\n",
    "\n",
    "It is common to start with one large set of data that you want to learn about and to split it into 80% training data set and 20% test data set."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6da2d094-2906-4ae4-afd1-bbddc9d398ed",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Backpropagation (\"the backward propagation of errors\")\n",
    "\n",
    "Once you have your network structure, your initial weights and your training data set, you can start training.\n",
    "\n",
    "There have been lots of algorithms to do this over the last several decades but the currently most popular one is *backpropagation*.\n",
    "\n",
    "First, we define our *loss function*, a measure of \"how wrong\" we are. For example, $J(y) = (t-y)^2$ where $y$ is the output of the network and $t$ is what we *want* the output to be.\n",
    "\n",
    "We then calculate its derivative with respect to each weight, $D_n(y) = \\frac{dJ(y)}{dw_n}$. This gives how much you need to tweak each weight—and in which direction—to correct the output.\n",
    "\n",
    "Then for each training entry:\n",
    "1. pass it through the network and find the value $y$\n",
    "2. tweak each weight by $\\delta w_n = -R D_n(y)$ where $R$ is the *learning rate*\n",
    " \n",
    "This means that the 'more wrong' the weights are, the more they move towards the true value. This slows down as, after lots of examples, the network *converges*."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a36fe8f9-1fa5-4d44-a77f-6616507a80cd",
   "metadata": {
    "tags": []
   },
   "source": [
    "<img src=\"backprop1.png\" alt=\"Back propagation example\" style=\"width: 100%; margin:0 auto;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03f7ecc0-7c88-4bc9-a92e-9686bb558ecd",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "## Common neural network libraries\n",
    "\n",
    "It would, as with with most things, be possible to to the above by hand but that would take years to make any progress. Instead we use software packages to do the leg work for us.\n",
    "\n",
    "The can in general, construct networks, automatically calculate derivatives, perform backpropagation and evaluate performance for you.\n",
    "\n",
    "Some of the most popular are:\n",
    "- PyTorch\n",
    "- TensorFlow\n",
    "- scikit-learn\n",
    "\n",
    "In this workshop, we will be using TensorFlow."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
