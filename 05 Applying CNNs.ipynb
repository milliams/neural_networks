{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "031ca53f",
   "metadata": {},
   "source": [
    "# Applying CNNs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ac3f94d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The MNIST data set is a collection of 70,000 28×28 pixel images of scanned, handwritten digits.\n",
    "\n",
    "![MNIST examples](MnistExamples.png)\n",
    "\n",
    "We want to create a network which can, given a similar image of a digit, identify its value."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f4462b9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Using TensorFlow to create and train a network\n",
    "\n",
    "In TensorFlow, there are three main tasks needed before you can start training. You must:\n",
    "\n",
    " 1. Specify the shape of your network\n",
    " 2. Specify how the network should be trained\n",
    " 3. Specify your training data set\n",
    " \n",
    "We will now go through each of these to show how the parts fit together."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20e4033c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Designing the CNN\n",
    "\n",
    "We will create a network which fits the following design:\n",
    "\n",
    " 1. **Convolutional Layer #1**: Applies 16 5×5 filters (extracting 5×5-pixel subregions), with [ReLU](https://en.wikipedia.org/wiki/Rectifier_(neural_networks) activation function\n",
    " 2. **Pooling Layer #1**: Performs max pooling with a 2×2 filter and stride of 2 (which specifies that pooled regions do not overlap)\n",
    " 3. **Convolutional Layer #2**: Applies 32 5×5 filters, with ReLU activation function\n",
    " 4. **Pooling Layer #2**: Again, performs max pooling with a 2×2 filter and stride of 2\n",
    " 5. **Dense Layer #1**: 128 neurons, with dropout regularization rate of 0.4 (probability of 40% that any given element will be dropped during training)\n",
    " 6. **Dense Layer #2 (Logits Layer)**: 10 neurons, one for each digit target class (0–9).\n",
    "\n",
    "This structure has been designed and tweaked specifically for the problem of classifying the MNIST data, however in general it is a good starting point for any similar image classification problem."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba91d757",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Building the CNN\n",
    "\n",
    "We're using TensorFlow to create our CNN but we're able to use the Keras API inside it to simplify the network construction. We build up our network sequentially, layer-by-layer.\n",
    "\n",
    "### First convolutional layer\n",
    "\n",
    "We start with our first convolutional layer. It create 16 5×5 filters. Since we have specified `padding=\"same\"`, the size of the layer will still be 28×28 but as we specified 16 filters, the overall size of the layer will be 28×28×16=12,544."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "0abf7884",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2024-01-19 10:51:20.717446: I external/local_tsl/tsl/cuda/cudart_stub.cc:31] Could not find cuda drivers on your machine, GPU will not be used.\n",
      "2024-01-19 10:51:20.754297: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered\n",
      "2024-01-19 10:51:20.754330: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered\n",
      "2024-01-19 10:51:20.755153: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered\n",
      "2024-01-19 10:51:20.760511: I external/local_tsl/tsl/cuda/cudart_stub.cc:31] Could not find cuda drivers on your machine, GPU will not be used.\n",
      "2024-01-19 10:51:21.771216: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT\n",
      "2024-01-19 10:51:22.934963: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:901] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero. See more at https://github.com/torvalds/linux/blob/v6.0/Documentation/ABI/testing/sysfs-bus-pci#L344-L355\n",
      "2024-01-19 10:51:22.940675: W tensorflow/core/common_runtime/gpu/gpu_device.cc:2256] Cannot dlopen some GPU libraries. Please make sure the missing libraries mentioned above are installed properly if you would like to use GPU. Follow the guide at https://www.tensorflow.org/install/gpu for how to download and setup the required libraries for your platform.\n",
      "Skipping registering GPU devices...\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "model = tf.keras.models.Sequential([\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=16,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb6d1d4c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### First pooling layer\n",
    "\n",
    "Next we add in a pooling layer. This reduces the size of the image by a factor of two in each direction (now effectively a 14×14 pixel image). This is important to reduce memory usage and to allow feature generalisation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "608eed77",
   "metadata": {},
   "outputs": [],
   "source": [
    "model = tf.keras.models.Sequential([\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=16,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "    # ↓ new lines ↓\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0090289a",
   "metadata": {},
   "source": [
    "After pooling, the layer size is 14×14×16=3136."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25c6f5fd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Second convolutional and pooling layers\n",
    "\n",
    "We then add in our second convolution and pooling layers which reduce the image size while increasing the width of the network so we can describe more features:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "19d67746",
   "metadata": {},
   "outputs": [],
   "source": [
    "model = tf.keras.models.Sequential([\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=16,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "    # ↓ new lines ↓\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=32,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf7ed455",
   "metadata": {},
   "source": [
    "After this final convolution and pooling, we have a layer of size 7×7×32=1568."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b3b6b82",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Fully-connected section\n",
    "\n",
    "Finally, we get to the fully-connected part of the network. At this point we no longer consider this an 'image' any more so we flatten our 3D layer into a linear set of nodes. We then add in a dense (fully-connected) layer with 128 neurons.\n",
    "\n",
    "To avoid over-fitting, we apply *dropout regularization* to our dense layer which causes it to randomly ignore 40% of the nodes each training cycle (to help avoid overfitting) before adding in our final layer which has 10 neurons which we expect to relate to each of our 10 classes (the numbers 0-9):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "fcc3b6b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "model = tf.keras.models.Sequential([\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=16,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "    tf.keras.layers.Conv2D(\n",
    "        filters=32,\n",
    "        kernel_size=5,\n",
    "        padding=\"same\",\n",
    "        activation=tf.nn.relu\n",
    "    ),\n",
    "    tf.keras.layers.MaxPool2D((2, 2), (2, 2), padding=\"same\"),\n",
    "    # ↓ new lines ↓\n",
    "    tf.keras.layers.Flatten(),\n",
    "    tf.keras.layers.Dense(128, activation=\"relu\"),\n",
    "    tf.keras.layers.Dropout(0.4),\n",
    "    tf.keras.layers.Dense(10, activation=\"softmax\")\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25de1c04",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Telling it how to train\n",
    "\n",
    "To finalise our model we once more use sparse, categorical cross-entropy as the loss function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3ab17211",
   "metadata": {},
   "outputs": [],
   "source": [
    "model.compile(\n",
    "    loss=\"sparse_categorical_crossentropy\",\n",
    "    metrics=[\"accuracy\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c18df1b7",
   "metadata": {},
   "source": [
    "Sparse categorical cross-entropy should be used when you have a classification problem and the labels being used are the index of the desired class."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c4db10a",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Getting the data into Python\n",
    "\n",
    "We've now finished designing our network so we can start getting our data into place. TensorFlow comes with a built-in loader for the MNIST dataset which has a pre-configured train/test split:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "bc049d25",
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import tensorflow_datasets as tfds\n",
    "\n",
    "ds_train, ds_test = tfds.load(\n",
    "    \"mnist\",\n",
    "    split=[\"train\", \"test\"],\n",
    "    as_supervised=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7fa02af",
   "metadata": {},
   "source": [
    "`ds_train` and `ds_test` are both sequences of 28×28×1 matrices containing the numbers 0-255. Each example also has a label associated with it which is a single integer scalar from 0-9."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "ab8233ec",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(TensorSpec(shape=(28, 28, 1), dtype=tf.uint8, name=None),\n",
       " TensorSpec(shape=(), dtype=tf.int64, name=None))"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds_train.element_spec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e8af4b9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Creating the training dataset\n",
    "\n",
    "The first thing we need to do with our data is convert it from being in the range 0-255 to being in the range 0.0-1.0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "529782df",
   "metadata": {},
   "outputs": [],
   "source": [
    "def normalize_img(image, label):\n",
    "    return tf.cast(image, tf.float32) / 255., label\n",
    "\n",
    "ds_train = ds_train.map(normalize_img)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9f682c8",
   "metadata": {},
   "source": [
    "Then we shuffle each complete input set and collect them into batches of 128:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "323c5f1d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_train = ds_train.shuffle(1000)\n",
    "ds_train = ds_train.batch(128)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2356f69",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Creating the test dataset\n",
    "\n",
    "In order for it to be a fair comparison, we need to do some of the same pre-processing to the test dataset too:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "4b9c4683",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_test = ds_test.map(normalize_img)\n",
    "ds_test = ds_test.batch(128)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf165376",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Fitting the model to the data\n",
    "\n",
    "At this point, we're all ready to go. We call the `fit` method on the model, passing in the training data, how long to run for and the test data set to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "371fc595",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/2\n",
      "469/469 [==============================] - 27s 57ms/step - loss: 0.2495 - accuracy: 0.9226 - val_loss: 0.0575 - val_accuracy: 0.9824\n",
      "Epoch 2/2\n",
      "469/469 [==============================] - 24s 52ms/step - loss: 0.0709 - accuracy: 0.9783 - val_loss: 0.0341 - val_accuracy: 0.9887\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<keras.src.callbacks.History at 0x7fa718cc6f10>"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model.fit(\n",
    "    ds_train,\n",
    "    validation_data=ds_test,\n",
    "    epochs=2,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a897a70a",
   "metadata": {},
   "source": [
    "## Testing the model in the real world\n",
    "\n",
    "Let's start by downloading some example image files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "090b1d2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from urllib.request import urlretrieve\n",
    "\n",
    "for i in list(range(1,10)) + [\"dog\"]:\n",
    "    urlretrieve(f\"https://github.com/milliams/intro_deep_learning/raw/master/{i}.png\", f\"{i}.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2be4db2f",
   "metadata": {},
   "source": [
    "Then, load them in and convert them to the same format as the data that we trained and validated on:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "48b639b8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(10, 28, 28, 1)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "from skimage.io import imread\n",
    "\n",
    "images = []\n",
    "for i in list(range(1,10)) + [\"dog\"]:\n",
    "    images.append(np.array(imread(f\"{i}.png\")/255.0, dtype=\"float32\"))\n",
    "images = np.array(images)[:,:,:,np.newaxis]\n",
    "images.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b567ca0f",
   "metadata": {},
   "source": [
    "`images` is in the same shape as our training and validation data. It's a 4D array of 10 images each 28×28 pixels with one colour channel. We can pass it directly to the `model.predict` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "78510a38",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1/1 [==============================] - 0s 92ms/step\n"
     ]
    }
   ],
   "source": [
    "probabilities = model.predict(images)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8746118",
   "metadata": {},
   "source": [
    "To summarise all the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "cc6afbca",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1 at  0.9%. CNN thinks it's a 3 (43.9%)\n",
      "2 at 93.8%. CNN thinks it's a 2 (93.8%)\n",
      "3 at 43.0%. CNN thinks it's a 3 (43.0%)\n",
      "4 at  3.5%. CNN thinks it's a 3 (94.2%)\n",
      "5 at 99.9%. CNN thinks it's a 5 (99.9%)\n",
      "6 at  5.9%. CNN thinks it's a 3 (38.9%)\n",
      "7 at 17.7%. CNN thinks it's a 3 (36.0%)\n",
      "8 at 47.1%. CNN thinks it's a 8 (47.1%)\n",
      "9 at 11.4%. CNN thinks it's a 8 (49.5%)\n",
      "dog. CNN thinks it's a 8 (48.2%)\n"
     ]
    }
   ],
   "source": [
    "truths = list(range(1, 10)) + [\"dog\"]\n",
    "\n",
    "table = []\n",
    "for truth, probs in zip(truths, probabilities):\n",
    "    prediction = probs.argmax()\n",
    "    if truth == 'dog':\n",
    "        print(f\"{truth}. CNN thinks it's a {prediction} ({probs[prediction]*100:.1f}%)\")\n",
    "    else:\n",
    "        print(f\"{truth} at {probs[truth]*100:4.1f}%. CNN thinks it's a {prediction} ({probs[prediction]*100:4.1f}%)\")\n",
    "    table.append((truth, probs))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f825adf0",
   "metadata": {},
   "source": [
    "Or, in table form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "6cee9e92",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": [
     "remove_input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table cellpadding=\"0\" style=\"border-collapse: collapse; border-style: hidden;\">\n",
       "    <thead>\n",
       "    <tr>\n",
       "    <td><b>Image</b></td><td><b>0</b></td><td><b>1</b></td><td><b>2</b></td><td><b>3</b></td><td><b>4</b></td><td><b>5</b></td><td><b>6</b></td><td><b>7</b></td><td><b>8</b></td><td><b>9</b></td></tr>\n",
       "    </thead>\n",
       "    <tbody><tr><td><img src=\"1.png\" style=\"margin: 1px 0px\"></td><td>5%</td><td>1%</td><td>6%</td><td style=\"color:red;\">44%</td><td>1%</td><td>5%</td><td>2%</td><td>1%</td><td>32%</td><td>4%</td></tr><tr><td><img src=\"2.png\" style=\"margin: 1px 0px\"></td><td>2%</td><td>0%</td><td style=\"color:green;\">94%</td><td>0%</td><td>1%</td><td>0%</td><td>3%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"3.png\" style=\"margin: 1px 0px\"></td><td>5%</td><td>0%</td><td>6%</td><td style=\"color:green;\">43%</td><td>0%</td><td>6%</td><td>7%</td><td>0%</td><td>32%</td><td>0%</td></tr><tr><td><img src=\"4.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:red;\">94%</td><td>4%</td><td>1%</td><td>0%</td><td>0%</td><td>0%</td><td>1%</td></tr><tr><td><img src=\"5.png\" style=\"margin: 1px 0px\"></td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td><td style=\"color:green;\">100%</td><td>0%</td><td>0%</td><td>0%</td><td>0%</td></tr><tr><td><img src=\"6.png\" style=\"margin: 1px 0px\"></td><td>3%</td><td>0%</td><td>1%</td><td style=\"color:red;\">39%</td><td>0%</td><td>35%</td><td>6%</td><td>0%</td><td>15%</td><td>0%</td></tr><tr><td><img src=\"7.png\" style=\"margin: 1px 0px\"></td><td>4%</td><td>2%</td><td>21%</td><td style=\"color:red;\">36%</td><td>1%</td><td>1%</td><td>7%</td><td>18%</td><td>9%</td><td>0%</td></tr><tr><td><img src=\"8.png\" style=\"margin: 1px 0px\"></td><td>3%</td><td>1%</td><td>4%</td><td>13%</td><td>3%</td><td>16%</td><td>10%</td><td>1%</td><td style=\"color:green;\">47%</td><td>3%</td></tr><tr><td><img src=\"9.png\" style=\"margin: 1px 0px\"></td><td>15%</td><td>0%</td><td>4%</td><td>6%</td><td>2%</td><td>4%</td><td>7%</td><td>0%</td><td style=\"color:red;\">50%</td><td>11%</td></tr><tr><td><img src=\"dog.png\" style=\"margin: 1px 0px\"></td><td>1%</td><td>1%</td><td>31%</td><td>9%</td><td>0%</td><td>3%</td><td>4%</td><td>1%</td><td style=\"color:red;\">48%</td><td>1%</td></tr></tbody>\n",
       "    </table>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import display, HTML\n",
    "\n",
    "def print_table(table):\n",
    "    out = \"\"\n",
    "    out += \"\"\"<table cellpadding=\"0\" style=\"border-collapse: collapse; border-style: hidden;\">\n",
    "    <thead>\n",
    "    <tr>\n",
    "    <td><b>Image</b></td>\"\"\"\n",
    "    for i in range(10):\n",
    "        out += f\"<td><b>{i}</b></td>\"\n",
    "    out += \"\"\"</tr>\n",
    "    </thead>\n",
    "    <tbody>\"\"\"\n",
    "    for truth, l in table:\n",
    "        out += \"<tr>\"\n",
    "        out += f'<td><img src=\"{truth}.png\" style=\"margin: 1px 0px\"></td>'\n",
    "        highest_prob = l.argmax()\n",
    "        for j, m in enumerate(l):\n",
    "            if j == highest_prob:\n",
    "                if highest_prob == truth:\n",
    "                    colour = \"green\"\n",
    "                else:\n",
    "                    colour = \"red\"\n",
    "                out += f'<td style=\"color:{colour};\">{int(round(m*100))}%</td>'\n",
    "            else:\n",
    "                out += f\"<td>{int(round(m*100))}%</td>\"\n",
    "        out += \"</tr>\"\n",
    "    out += \"\"\"</tbody>\n",
    "    </table>\"\"\"\n",
    "    return HTML(out)\n",
    "\n",
    "print_table(table)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5f568b1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You should see that `5` seem to have worked well maybe some others have the correct answer with a low probability but most are struggling.\n",
    "\n",
    "Your results will likely be different but they will probably have the same strengths and weaknesses. Can you see why some have predicted well, and other have not?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f82cfeea",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Are you able to improve on the performance, either on the validation data set (`val_accuracy`), or on the examples, by tweaking the design of the network?\n",
    "\n",
    "Are you able to get equivalent performance with a simpler network (fewer filters, fewer convolutional layers, fewer dense neurons)? Do you get better performance with a more complex network?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
